<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class BaseController extends Controller
{
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendResponse($result, $message = '')
    {
        $response = [
            'success' => true,
            'code'    => 200,
            'message' => $message,
            'result'  => $result,
        ];

        return response()->json($response, 200);
    }

    /**
     * return error response.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendError($error, $errorMessages = [], $code = 404)
    {
        $response = [
            'success' => false,
            'code'    => $code,
            'message' => $error,
            'result'  => !empty($errorMessages) ? $errorMessages : null
        ];

        return response()->json($response, $code);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function paginate($items, $perPage = 10, $page = null, $options = [])
    {
        // $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        // $items = $items instanceof Collection ? $items : Collection::make($items);
        // return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);


        // $items = array();
        //     foreach(Thread::all() as $thread)
        //     {
        //         if($thread->hasCorrectAnswer())
        //         {
        //             array_push($items, $thread);
        //         }
        //     }

        $currentPage    = LengthAwarePaginator::resolveCurrentPage();
        $currentItems   = array_slice($items, $perPage * ($currentPage - 1), $perPage);
        return new LengthAwarePaginator($currentItems, count($items), $perPage, $currentPage);

        // $results = $paginator->appends('filter', request('filter'));
        // break;
    }
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class BannerController extends BaseController
{
  public function __construct()
  {
    // $this->middleware('auth:api', ['except' => ['login', 'refresh', 'logout']]);
  }
  // /**
  //  * Instantiate a new UserController instance.
  //  */
  // public function __construct()
  // {
  //     date_default_timezone_set("Asia/Jakarta");
  // }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $banner = DB::table('banner')->get();
    return response()->json($banner);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    try {
      $input = $request->all();


      $validator = Validator::make($input, [
        "title"     => 'required',
        "desc"      => 'required',
        "image"     => 'required|image|mimes:jpg,jpeg,png',
        "id_category" => 'required',
      ]);

      if ($validator->fails()) {
        return $this->sendError($validator->errors());
      }

      if (!$request->hasFile('image')) {
        return $this->sendError('Upload Image Not Found.');
      }

      $file = $request->file('image');

      if (!$file->isValid()) {
        return $this->sendError('Invalid image file.');
      }

      $maxSize = 5 * 1024 * 1024; // Batas ukuran file 5MB
      if ($file->getSize() > $maxSize) {
        return $this->sendError('File size exceeds the maximum allowed size.');
      }

      $image = time() . '.' . $file->getClientOriginalExtension();
      $file->move(base_path('public/uploads/banner'), $image);

      $id_category = DB::table('category_banner')->where('id', $request->id_category)->first();
      // $id_ticket = DB::table('ticket')->where('id', $request->id_ticket)->first();

      if (!$id_category) {
        return $this->sendError('Invalid id_category.');
      }
      // if (!$id_ticket) {
      //   return $this->sendError('Invalid id_ticket.');
      // }

      $from = array(
        'title'             => $request->title,
        'desc'              => $request->desc,
        'image'             => $image,
        'slug'              => Str::slug($request->title, '-'),
        'id_category'       => $id_category->id,
        'id_ticket'         => $request->id_ticket,
        'is_ticket'         => !empty($request->id_ticket),
        'flag'              => 1,
        'created_by'        => 1,
        'created_at'        => date('Y-m-d H:i:s')
      );
      DB::table('banner')->insertGetId($from);
      return $this->sendResponse($from, 'Banner created successfully.');
    } catch (\Throwable $th) {
      return $this->sendError($th->getMessage());
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function detail($slug = null)
  {
    try {
      $result = [];
      $data   = DB::table('banner')->where('slug', $slug)->first();

      if (is_null($data)) {
        return $this->sendError('Banner not found.');
      }

      foreach ($data as $key => $value) :
        if ($key == 'image') :
          $result[$key] = url('/') . '/uploads/banner/' . $value;
        elseif (!in_array($key, ['flag', 'created_by', 'updated_at', 'deleted_at'])) :
          $result[$key] = $value;
        endif;
      endforeach;

      return $this->sendResponse($result, 'Banner by Detail retrieved successfully.');
    } catch (\Exception $th) {
      return $this->sendError($th->getMessage());
    }
  }

  public function showById($id)
  {
    try {
      $result = [];
      $data   = DB::table('banner')->where('id', $id)->first();

      if (is_null($data)) {
        return $this->sendError('Banner not found.');
      }

      foreach ($data as $key => $value) :
        if ($key == 'image') :
          $result[$key] = url('/') . '/uploads/banner/' . $value;
        elseif (!in_array($key, ['flag', 'created_by', 'updated_at', 'deleted_at'])) :
          $result[$key] = $value;
        endif;
      endforeach;

      return $this->sendResponse($result, 'Banner by Detail retrieved successfully.');
    } catch (\Exception $th) {
      return $this->sendError($th->getMessage());
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id = null)
  {
    try {
      $input = $request->all();

      $validator = Validator::make($input, [
        "title"     => 'required',
        "desc"      => 'required',
        "image"     => 'required|image|mimes:jpg,jpeg,png',
        "id_category" => 'required',
        "id_ticket" => 'required',
        "is_ticket" => 'required|in:0,1'
      ]);

      if ($validator->fails()) {
        return $this->sendError($validator->errors()->first());
      }

      $image = null;

      if ($request->hasFile('image')) {
        $file = $request->file('image');
        if ($file->isValid()) {
          $maxSize = 5 * 1024 * 1024; // Batas ukuran file 5MB
          if ($file->getSize() > $maxSize) {
            return $this->sendError('File size exceeds the maximum allowed size.');
          }
          $image  = time() . '.' . $file->getClientOriginalExtension();
          $file->move(base_path('public/uploads/banner'), $image);
        } else {
          return $this->sendError('Invalid image file.');
        }
      }

      $from = array(

        'title'             => $request->title,
        'desc'              => $request->desc,
        'id_category'       => $request->id_category,
        'id_ticket'         => $request->id_ticket,
        'is_ticket'         => $request->is_ticket,
        'slug'              => Str::slug($request->title, '-'),
        'updated_at'        => date('Y-m-d H:i:s')
      );

      if ($image !== null) {
        $from['image'] = $image;
      }

      DB::table('banner')->where('id', $id)->update($from);

      return $this->sendResponse($from, 'Banner updated successfully.');
    } catch (\Exception $th) {
      return $this->sendError($th->getMessage());
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request, $id = null)
  {
    try {
      $form = array(
        'flag'              => 0
      );

      DB::table('banner')->where('id', $id)->update($form);

      return $this->sendResponse($form, 'banner deleted successfully.');
    } catch (\Throwable $th) {
      return $this->sendError($th->getMessage());
    }
  }
}

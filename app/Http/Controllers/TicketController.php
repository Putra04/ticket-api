<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class TicketController extends BaseController
{
  public function __construct()
  {
    // $this->middleware('auth:api', ['except' => ['login', 'refresh', 'logout']]);
  }
  // /**
  //  * Instantiate a new UserController instance.
  //  */
  // public function __construct()
  // {
  //     date_default_timezone_set("Asia/Jakarta");
  // }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $ticket = DB::table('ticket')->get();
    return response()->json($ticket);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    try {
      $input = $request->all();


      $validator = Validator::make($input, [
        "title"     => 'required',
        "desc"      => 'required',
        "location"      => 'required',
        "date"      => 'required',
        "price"      => 'required',
        "image_cover"     => 'required|image|mimes:jpg,jpeg,png',
        "toc"      => 'required',
        "id_category_ticket" => 'required',
        "maps_embed" => 'required',
      ]);

      if ($validator->fails()) {
        return $this->sendError($validator->errors());
      }

      if (!$request->hasFile('image_cover')) {
        return $this->sendError('Upload Image Not Found.');
      }

      $file = $request->file('image_cover');

      if (!$file->isValid()) {
        return $this->sendError('Invalid image_cover file.');
      }

      $maxSize = 5 * 1024 * 1024; // Batas ukuran file 5MB
      if ($file->getSize() > $maxSize) {
        return $this->sendError('File size exceeds the maximum allowed size.');
      }

      $image_cover = time() . '.' . $file->getClientOriginalExtension();
      $file->move(base_path('public/uploads/ticket'), $image_cover);

      $id_category_ticket = DB::table('category_ticket')->where('id', $request->id_category_ticket)->first();

      if (!$id_category_ticket) {
        return $this->sendError('Invalid id_category.');
      }

      $from = array(
        'title'             => $request->title,
        'desc'              => $request->desc,
        'location'              => $request->locatioin,
        'date'              => $request->date,
        'price'              => $request->price,
        'image_cover'             => $image_cover,
        'slug'              => Str::slug($request->title, '-'),
        'toc'              => $request->toc,
        'id_category_ticket'       => $id_category_ticket->id,
        'maps_embed'              => $request->maps_embed,
        'flag'              => 1,
        'created_by'        => 1,
        'created_at'        => date('Y-m-d H:i:s')
      );
      DB::table('ticket')->insertGetId($from);
      return $this->sendResponse($from, 'Ticket created successfully.');
    } catch (\Throwable $th) {
      return $this->sendError($th->getMessage());
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function detail($slug = null)
  {
    try {
      $result = [];
      $data   = DB::table('ticket')->where('slug', $slug)->first();

      if (is_null($data)) {
        return $this->sendError('Ticket not found.');
      }

      foreach ($data as $key => $value) :
        if ($key == 'image_cover') :
          $result[$key] = url('/') . '/uploads/ticket/' . $value;
        elseif (!in_array($key, ['flag', 'created_by', 'updated_at', 'deleted_at'])) :
          $result[$key] = $value;
        endif;
      endforeach;

      return $this->sendResponse($result, 'Ticket by Detail retrieved successfully.');
    } catch (\Exception $th) {
      return $this->sendError($th->getMessage());
    }
  }

  public function showById($id)
  {
    try {
      $result = [];
      $data   = DB::table('ticket')->where('id', $id)->first();

      if (is_null($data)) {
        return $this->sendError('Ticket not found.');
      }

      foreach ($data as $key => $value) :
        if ($key == 'image_cover') :
          $result[$key] = url('/') . '/uploads/ticket/' . $value;
        elseif (!in_array($key, ['flag', 'created_by', 'updated_at', 'deleted_at'])) :
          $result[$key] = $value;
        endif;
      endforeach;

      return $this->sendResponse($result, 'Ticket by Detail retrieved successfully.');
    } catch (\Exception $th) {
      return $this->sendError($th->getMessage());
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id = null)
  {
    try {
      $input = $request->all();

      $validator = Validator::make($input, [
        "title"     => 'required',
        "desc"      => 'required',
        "location"      => 'required',
        "date"      => 'required',
        "price"      => 'required',
        "image_cover"     => 'required|image|mimes:jpg,jpeg,png',
        "toc"      => 'required',
        "id_category_ticket" => 'required',
        "maps_embed" => 'required',
      ]);

      if ($validator->fails()) {
        return $this->sendError($validator->errors()->first());
      }

      $image_cover = null;

      if ($request->hasFile('image_cover')) {
        $file = $request->file('image_cover');
        if ($file->isValid()) {
          $maxSize = 5 * 1024 * 1024; // Batas ukuran file 5MB
          if ($file->getSize() > $maxSize) {
            return $this->sendError('File size exceeds the maximum allowed size.');
          }
          $image_cover  = time() . '.' . $file->getClientOriginalExtension();
          $file->move(base_path('public/uploads/ticket'), $image_cover);
        } else {
          return $this->sendError('Invalid image cover file.');
        }
      }

      $from = array(

        'title'             => $request->title,
        'desc'              => $request->desc,
        'location'              => $request->locatioin,
        'date'              => $request->date,
        'price'              => $request->price,
        'slug'              => Str::slug($request->title, '-'),
        'toc'              => $request->toc,
        'id_category_ticket'       => $id_category_ticket->id,
        'maps_embed'              => $request->maps_embed,
        'updated_at'        => date('Y-m-d H:i:s')
      );

      if ($image_cover !== null) {
        $from['image_cover'] = $image_cover;
      }

      DB::table('ticket')->where('id', $id)->update($from);

      return $this->sendResponse($from, 'Ticket updated successfully.');
    } catch (\Exception $th) {
      return $this->sendError($th->getMessage());
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request, $id = null)
  {
    try {
      $form = array(
        'flag'              => 0
      );

      DB::table('ticket')->where('id', $id)->update($form);

      return $this->sendResponse($form, 'Ticket deleted successfully.');
    } catch (\Throwable $th) {
      return $this->sendError($th->getMessage());
    }
  }
}

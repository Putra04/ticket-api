<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class CategoryBannerController extends BaseController
{
  public function __construct()
  {
    // $this->middleware('auth:api', ['except' => ['login', 'refresh', 'logout']]);
  }
  // /**
  //  * Instantiate a new UserController instance.
  //  */
  // public function __construct()
  // {
  //     date_default_timezone_set("Asia/Jakarta");
  // }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $category_banner = DB::table('category_banner')->get();
    return response()->json($category_banner);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    try {
      $input = $request->all();


      $validator = Validator::make($input, [
        "name"     => 'required',
      ]);

      if ($validator->fails()) {
        return $this->sendError($validator->errors());
      }

      $from = array(
        'name'             => $request->name,
        'flag'              => 1,
        'created_by'        => 1,
        'created_at'        => date('Y-m-d H:i:s')
      );
      DB::table('category_banner')->insertGetId($from);
      return $this->sendResponse($from, 'Category Banner created successfully.');
    } catch (\Throwable $th) {
      return $this->sendError($th->getMessage());
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function detail($id)
  {
    try {
      $result = [];
      $data   = DB::table('category_banner')->where('id', $id)->first();

      if (is_null($data)) {
        return $this->sendError('Category Banner not found.');
      }

      foreach ($data as $key => $value) :
        if ($key == 'image') :
          $result[$key] = url('/') . '/uploads/banner/' . $value;
        elseif (!in_array($key, ['flag', 'created_by', 'updated_at', 'deleted_at'])) :
          $result[$key] = $value;
        endif;
      endforeach;

      return $this->sendResponse($result, 'Category Banner by Detail retrieved successfully.');
    } catch (\Exception $th) {
      return $this->sendError($th->getMessage());
    }
  }

  public function showById($id)
  {
    try {
      $result = [];
      $data   = DB::table('category_banner')->where('id', $id)->first();

      if (is_null($data)) {
        return $this->sendError('Category Banner not found.');
      }

      foreach ($data as $key => $value) :
        if ($key == 'image') :
          $result[$key] = url('/') . '/uploads/banner/' . $value;
        elseif (!in_array($key, ['flag', 'created_by', 'updated_at', 'deleted_at'])) :
          $result[$key] = $value;
        endif;
      endforeach;

      return $this->sendResponse($result, 'Category Banner by Detail retrieved successfully.');
    } catch (\Exception $th) {
      return $this->sendError($th->getMessage());
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id = null)
  {
    try {
      $input = $request->all();

      $validator = Validator::make($input, [
        "name"     => 'required',
      ]);

      if ($validator->fails()) {
        return $this->sendError($validator->errors()->first());
      }

      $from = array(

        'name'             => $request->name,
        'updated_at'        => date('Y-m-d H:i:s')
      );

      DB::table('category_banner')->where('id', $id)->update($from);

      return $this->sendResponse($from, 'Category Banner updated successfully.');
    } catch (\Exception $th) {
      return $this->sendError($th->getMessage());
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request, $id = null)
  {
    try {
      $form = array(
        'flag'              => 0
      );

      DB::table('category_banner')->where('id', $id)->update($form);

      return $this->sendResponse($form, 'Category Banner deleted successfully.');
    } catch (\Throwable $th) {
      return $this->sendError($th->getMessage());
    }
  }
}

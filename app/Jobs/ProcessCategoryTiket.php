<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class ProcessCategoryTicket implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $categoryName;

    public function __construct($categoryName)
    {
        $this->categoryName = $categoryName;
    }

    public function handle()
    {
        // Lakukan sesuatu dengan $this->categoryName, misalnya simpan ke database
        DB::table('category_ticket')->insert([
            'name' => $this->categoryName,
            'flag' => 1,
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}

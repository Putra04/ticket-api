<?php

use Illuminate\Support\Facades\Redis;

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

// $router->get('/', function () {
//   $redis = Redis::incr('p');
//   return $redis;
// });

$router->get('/tes', 'CategoryTicketController@cache');

// BANNER
$router->get('/banner', 'BannerController@index');
$router->post('banner/add', 'BannerController@store');

// CATEGORY BANNER
$router->get('/category-banner', 'CategoryBannerController@index');
$router->post('category-banner/add', 'CategoryBannerController@store');

// CATEGORY TICKET
$router->get('/category-ticket', 'CategoryTicketController@index');
$router->post('category-ticket/add', 'CategoryTicketController@store');

// TICKET
$router->get('/ticket', 'TicketController@index');
$router->post('ticket/add', 'TicketController@store');